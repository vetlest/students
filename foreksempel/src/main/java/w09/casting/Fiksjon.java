package w09.casting;

public interface Fiksjon {	
	default String faktaOmFiksjon() {
		// https://no.wikipedia.org/wiki/Fiksjon
		return "Fiksjon (fra latin fictionem (nominativ fictio), �forme, forestille seg, dikte opp�, jamf�r engelske fiction)[1][2] har betydningen �det som er oppdiktet�.";
	}
}
