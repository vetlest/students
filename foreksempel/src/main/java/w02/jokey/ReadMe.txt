TDT4100 - Objektorientert programmering
JOKEY - HVA ER VITSEN?

Hvis denne er vanskelig � lese med lange linjer sett p� Word Wrap (i Windows Alt+Shift+Y)

README
Jokey er en applikasjon som kan vise vitser. Vi skal utvikle denne utover semesteret. 
Applikasjonen vil bli stadig utvidet med blant annet � kunne vise vitser fra flere ulike 
kilder og vise praktisk bruk av ulike objektorienterte teknikker. Applikasjonen vil 
gjennom typiske problemstillinger gi motivasjon for hvorfor en b�r benytte de ulike 
objektorienterte teknikkene og i hvilke tilfeller.

Hver utgave har en readme.txt fil hvor det st�r hva som er nytt siden forrige utgave. 
Hver utgave har en nummerering som har formatet "v[uke][versjon for uken]", feks "v0201" 
referer til versjon 1 i uke 2. Hver versjon bygger p� forrige versjon med enten 
utvidelser eller forbedringer/endringer. Endringene er beskrevet i denne filen.

UTGAVER
** v0201 **
Dette er F�rste utgave. Her lager vi bare en enkel klasse og viser hvordan vi kan kj�re 
denne klassen og skrive ut noen vitser til konsollet.

** v0202 **
Dette er da den andre utgave hvor vi har gjort om det forrige eksemplet til � bli mer 
objektorientert, dvs vi har laget en egen klasse for en vits og en egen klasse for 
vitsene. Vi har ogs� lagt inn noen metoder som lar oss benytte de nye klassene for � 
hente og vise vitser. I tillegg er det lagt inn noen elementer for viderekommende.
